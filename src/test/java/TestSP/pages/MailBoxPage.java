package TestSP.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MailBoxPage {

    public MailBoxPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver driver;

    private WebElement titleWithAccountName() {
        return new WebDriverWait(driver, 4).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@title,'Аккаунт Google')]")));
    }

    public String getTitleWithAccountName() {
        return titleWithAccountName().getAttribute("title");
    }


}
