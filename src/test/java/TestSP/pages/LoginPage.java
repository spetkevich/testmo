package TestSP.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    private WebDriver driver;

    public void open() {
        driver.get("https://mail.google.com");
    }

    private WebElement loginField() {
        return new WebDriverWait(driver, 4).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='email']")));
    }

    private WebElement passwordField() {

        return new WebDriverWait(driver, 4).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='password']//input")));
    }

    private WebElement nextButton() {
        return new WebDriverWait(driver, 4).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[.='Далее']")));
    }

    private WebElement accountError() {
        return new WebDriverWait(driver, 4).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@aria-live='assertive']")));
    }

    private WebElement passwordError() {
        return new WebDriverWait(driver, 4).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='password']//div[@aria-live='assertive']")));
    }

        public void inputLogin(String login) {
        loginField().sendKeys(login);
    }

    public void inputPassword(String password) {
        passwordField().sendKeys(password);
    }

    public void clickNext() {
        nextButton().click();
    }

    public String getAccountErrorText() {
        return accountError().getText();
    }

    public String getPasswordErrorText() {
        return passwordError().getText();
    }


}
