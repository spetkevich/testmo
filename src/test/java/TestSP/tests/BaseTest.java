package TestSP.tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    static WebDriver myDriver;

    @BeforeClass
    public static void initDriver() {
        System.setProperty("webdriver.chrome.driver", "./src/test/java/TestSP/resources/chromedriver.exe");
        myDriver = new ChromeDriver();
        myDriver.manage().window().maximize();
        myDriver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDown() {
        if (myDriver != null) {
            myDriver.quit();
        }
    }

}
