package TestSP.tests;

import TestSP.pages.LoginPage;
import TestSP.pages.MailBoxPage;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


public class GmailPositiveTest extends BaseTest {

    private static LoginPage loginPage;
    private static MailBoxPage mailBoxPage;

    @BeforeClass
    public static void beforeClass() {
        loginPage = new LoginPage(myDriver);
        mailBoxPage = new MailBoxPage(myDriver);
        loginPage.open();
    }

    @Test
    public void correctLoginAndPassword() {
        loginPage.inputLogin("testSP.AOC@gmail.com");
        loginPage.clickNext();
        loginPage.inputPassword("sp123456AOC");
        loginPage.clickNext();
        String titleWithAccountName = mailBoxPage.getTitleWithAccountName();
        Assert.assertTrue(titleWithAccountName.contains("Стас Петкевич"));
    }

}


