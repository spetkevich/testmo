package TestSP.tests;

import TestSP.pages.LoginPage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GmailNegotiveTest extends BaseTest {

    private static LoginPage loginPage;

    @BeforeClass
    public static void beforeClass() {
        loginPage = new LoginPage(myDriver);
    }

    @Before
    public void before() {
        loginPage.open();
    }

    @Test
    public void wrongLogin() {
        loginPage.inputLogin("wrong_testSP.AOC@gmail.com");
        loginPage.clickNext();
        String errorText = loginPage.getAccountErrorText();
        Assert.assertTrue(errorText.contains("Не удалось найти аккаунт Google"));
    }

    @Test
    public void wrongPassword() {
        loginPage.inputLogin("testSP.AOC@gmail.com");
        loginPage.clickNext();
        loginPage.inputPassword("wrong");
        loginPage.clickNext();
        String errorText = loginPage.getPasswordErrorText();
        Assert.assertTrue(errorText.contains("Неверный пароль. Повторите попытку или нажмите на ссылку \"Забыли пароль?\", чтобы сбросить его."));
    }

    @Test
    public void passwordInsteadLogin() {
        loginPage.inputLogin("sp123456AOC");
        loginPage.clickNext();
        String errorText = loginPage.getAccountErrorText();
        Assert.assertTrue(errorText.contains("Не удалось найти аккаунт Google"));
    }

    @Test
    public void loginInsteadPassword() {
        loginPage.inputLogin("testSP.AOC@gmail.com");
        loginPage.clickNext();
        loginPage.inputPassword("testSP.AOC@gmail.com");
        loginPage.clickNext();
        String errorText = loginPage.getPasswordErrorText();
        Assert.assertTrue(errorText.contains("Неверный пароль. Повторите попытку или нажмите на ссылку \"Забыли пароль?\", чтобы сбросить его."));
    }

}
